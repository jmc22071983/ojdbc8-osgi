OSGi wrapper for Oracle Database 18c JDBC Driver (Java 8)
==============================================

Overview
--------

This Maven project produces OSGi wrapper for Oracle Database 18c Express Edition JDBC Driver.

Build instructions
------------------

The build depends on the official Oracle JDBC driver JAR file provided by [Oracle](http://www.oracle.com/). 

To build the bundle for the Oracle JDBC driver for Java 8, follow the steps below.

1. Download the desired driver [**ojdbc8.jar**](https://download.oracle.com/otn/utilities_drivers/jdbc/183/ojdbc8.jar) for Oracle 18c (18.3.0.0.0).
2. Put ojdbc.jar in the same directory as the pom.xml.
3. Edit the POM to reflect the Oracle RDBMS version.
4. Install the driver to the local Maven repository.

        mvn install:install-file -Dfile=ojdbc8.jar -DgroupId=com.oracle -DartifactId=ojdbc8 -Dversion=18.3.0.0.0 -Dpackaging=jar

5. Build and install the OSGi wrapped driver bundle.

        mvn install
6. Install the OSGI Bundle in Felix Console.

7. Go to System Console and configure a new pool connection in the OSGi Service Pool **Day Commons JDBC Connections Pool** with:
																						

	JDBC driver class: oracle.jdbc.driver.OracleDriver
	
	JDBC connection URI: jdbc:oracle:thin:@localhost:1521:xe (your bbdd connection)
	
	Username and Password
	
	Validation query: SELECT 1 FROM DUAL
	
	Datasource name: For example "**oraclexe**"
	
8. Now you can use com.day.commons.datasource.poolservice.DataSourcePool in your Java Class to retrieve the configuration

	For example:
	 
	 @ Reference
	 private DataSourcePool dataSourceService;
	 
		public String testService() {
			try {
				 DataSource dataSource = (DataSource) dataSourceService.getDataSource("oraclexe");
				 Connection conn = dataSource.getConnection();
				 if (conn != null) {
					 return "Data Base connection success";
				 }
			} catch (Exception e) {
				LOG.error("SQLException, {}", e.getMessage());
			 }
			return "Data Base connection Error";
		}



